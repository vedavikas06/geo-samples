import matplotlib.pyplot as plt
import pandas as pd

dr = ['single_drug_perturbations-DM.csv', 'single_drug_perturbations-v1.0.csv', 'single_drug_perturbations-p1.0.csv']
di = ['disease_signatures-p1.0.csv', 'disease_signatures-v1.0.csv']
g = ['single_gene_perturbations-v1.0.csv', 'single_gene_perturbations-p1.0.csv']

l=[]
for d in dr:
    df = pd.read_csv(d)
    df = df.drop_duplicates(subset=['geo_id'])
    x = df['geo_id'].values
    for j in x:
        l.append(j)

print(len(l))
l.clear()
for d in di:
    df = pd.read_csv(d)
    df = df.drop_duplicates(subset=['geo_id'])
    x = df['geo_id'].values
    for j in x:
        l.append(j)

print(len(l))
l.clear()
for d in g:
    df = pd.read_csv(d)
    df = df.drop_duplicates(subset=['geo_id'])
    x = df['geo_id'].values
    for j in x:
        l.append(j)

print(len(l))

num = [716,655,3258]
lab = ['Drug\n716', 'Disease\n655', 'Gene\n3258']
col=['c','g','r']
plt.pie(num, labels=lab, colors=col, startangle=90, autopct='%1.1f%%')
plt.show()