import pandas as pd
import numpy as np
rna_GSE = []

cnt=0

with open("gds_result.txt") as openfile:
    for line in openfile:
        phrases = line.split()
        i = 0
        while i < len(phrases):
            if 'Accession:' in phrases[i]:
                cnt+=1
                print("Accession {0}:".format(cnt) + str(phrases[i + 1]))
                rna_GSE.append(phrases[i + 1])
                
            i += 1
df_all = pd.DataFrame({'GSEID': rna_GSE})
df_all.to_csv('rna_GSE.csv',index=False) 


df = pd.read_csv('rna_GSE.csv')
print(df.head())
rna_GSE = df.GSEID.values
print(rna_GSE)


import GEOparse
import time
import os

start = time.time()

GSM_Name,GSM_Title=[],[]
cnt = 0
for sample in rna_GSE[:1000]:
    gse = GEOparse.get_GEO(geo=sample.strip(), destdir="rna/")
    for gsm_name, gsm in gse.gsms.items():
        GSM_Name.append(gsm_name)
        GSM_Title.append(gsm.metadata['title'][0])
    if os.path.exists('rna/'+ sample.strip()+'_family.soft.gz'):
        os.remove('rna/'+ sample.strip()+'_family.soft.gz')
    cnt+=1
    print('--------------------------sample :--------------------'+ str(cnt))
    if cnt%100 == 0:
        df_all = pd.DataFrame({'GSM_Name': GSM_Name,'GSM_Title': GSM_Title})
        df_all.to_csv('rna_GSM_mapping.csv',index=False) 
        
print(GSM_Name)
print(GSM_Title)

end = time.time()
print('Runtime : ' + str(end - start) + ' sec')
