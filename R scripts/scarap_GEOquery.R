library(GEOquery)
GSE_Name = NULL
GSM_Name = NULL
GSM_Title = NULL
GSM_type = NULL


dataList = read.csv("microarray_GSE.csv")
dataList[, 'GSEID'] <- sapply(dataList[, 'GSEID'], as.character)
show(dataList[, 'GSEID'])

cnt <- 0

for(i in dataList[, 'GSEID']){
  
  tryCatch(
    # This is what I want to do...
    {
      print(i)
      eList <- getGEO(i,GSEMatrix=TRUE,destdir="microarray/",getGPL=FALSE)
      #show(eList)
      sub_data <- pData(phenoData(eList[[1]]))[c(1,2,6)]
      av1 <- as.vector(unlist(sub_data[[2]]))
      av2 <- as.vector(unlist(sub_data[[1]]))
      av3 <- as.vector(unlist(sub_data[[3]]))
      
      GSM_Name <- append(GSM_Name, av1)
      GSM_Title <- append(GSM_Title,av2)
      GSM_type <- append(GSM_type, av3)
      GSE_Name <- append(GSE_Name,replicate(length(sub_data[[2]]), i))
      #print(GSM_Title)
      #print(GSM_type)
      
      unlink("microarray/*")
      
      cnt <- cnt + 1
      
      if(cnt %% 100 == 0){
        print(paste0("------------------------------ sample : -------------------------", cnt))
        m <- cbind(GSM_Name, GSM_Title, GSM_type,GSE_Name)
        df <- as.data.frame(m)
        write.csv(df, file = "microarray_mapping_R.csv")
      }
    },
    # ... but if an error occurs, tell me what happened: 
    error=function(error_message) {
      message(error_message)
      return(NA)
    }
  )
  
}


m <- cbind(GSM_Name, GSM_Title, GSM_type,GSE_Name)
df <- as.data.frame(m)
write.csv(df, file = "microarray_mapping_R.csv")

